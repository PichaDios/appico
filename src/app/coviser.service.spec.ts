import { TestBed } from '@angular/core/testing';

import { CoviserService } from './coviser.service';

describe('CoviserService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CoviserService = TestBed.get(CoviserService);
    expect(service).toBeTruthy();
  });
});
