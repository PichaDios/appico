import { Component } from '@angular/core';
import { CoviserService } from '../coviser.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  summary : any[] = [];
  lastDate : null;
  newCountries : any;

  constructor(
    public service : CoviserService
  ) {}

  ngOnInit(){
  
  }

  ionViewWillEnter(){
    this.service.getSummary()
    .subscribe(
      (data) => { 
        this.summary = data['Global'];
        this.lastDate = data['Date'];
        this.newCountries = this.getNewData(data['Countries'], this.service.getSelectedCountries());
        console.log(this.newCountries);
      },
      (error) => {
        console.error(error);
      }
    )
  }

  getNewData(countries, selected){
    let new_data = [];
    console.log('for');
    for (let i = 0; i <selected.length;i++){
      console.log('i');
      for(let n = 0; i<countries.length;n++){
        console.log('n');
        if (selected[i]['ISO2'] == countries[n]['CountryCode']){

          console.log('entre buen');
          new_data.push(countries[n]);
          break;
        }
      }
    }
    return new_data;
  }
}