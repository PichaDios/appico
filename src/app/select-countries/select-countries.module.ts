import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectCountriesPageRoutingModule } from './select-countries-routing.module';

import { SelectCountriesPage } from './select-countries.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectCountriesPageRoutingModule
  ],
  declarations: [SelectCountriesPage]
})
export class SelectCountriesPageModule {}
