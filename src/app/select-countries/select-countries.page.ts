import { Component, OnInit } from '@angular/core';
import { CoviserService } from '../coviser.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-select-countries',
  templateUrl: './select-countries.page.html',
  styleUrls: ['./select-countries.page.scss'],
})
export class SelectCountriesPage implements OnInit {

  countries: any;

  constructor( 
    public service: CoviserService,
    public router: Router
  ) { }

  ngOnInit() {
    this.service.getCountries()
    .subscribe(
      (data) => {
        this.countries = data;
        this.countries.sort((a,b) => a.Country.toLowerCase() > b.Country.toLowerCase()? 1 : -1);
        console.log (this.countries);
      },
      (error) => {
        console.error(error);
      }
    )
  }

  onSubmit(){
    let data = [];
    this.countries.forEach(element => {
      if (element.isChecked)
        data.push(element);
    });
    this.service.setSelectedCountries(data);
    this.router.navigate(['/home']);
  }
}
